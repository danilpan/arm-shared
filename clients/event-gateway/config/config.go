package config

import (
	"context"
	"fmt"
	micro_config "go.unistack.org/micro/v3/config"
	"os"
)

type EventGatewayConfig interface {
	Load(ctx context.Context, option ...micro_config.LoadOption) error
	Addr() string
	DSN() string
	MaxIdleConns() int
	MaxOpenConns() int
	ConnMaxIdleTimeSeconds() int
	Name() string
	Init(opts ...micro_config.Option) error
	Options() micro_config.Options
	Save(ctx context.Context, option ...micro_config.SaveOption) error
	Watch(ctx context.Context, option ...micro_config.WatchOption) (micro_config.Watcher, error)
	String() string
}

type ClientConfig struct {
	Addr string `json:"addr" yaml:"addr"`
}

type config struct {
	ClientAddr string
	Database   string
	User       string
	Password   string
	Host       string
	Port       int
}

func NewEventGatewayConfig() EventGatewayConfig {
	return &config{}
}

func (c *config) Name() string {
	return "event-gateway-config"
}

func (c *config) Init(opts ...micro_config.Option) error {
	return nil
}

func (c *config) Options() micro_config.Options {
	return micro_config.Options{}
}

func (c *config) Save(ctx context.Context, option ...micro_config.SaveOption) error {
	return nil
}

func (c *config) Watch(ctx context.Context, option ...micro_config.WatchOption) (micro_config.Watcher, error) {
	return nil, nil
}

func (c *config) String() string {
	return "event-gateway-config"
}

func (c *config) Load(ctx context.Context, option ...micro_config.LoadOption) error {
	addr := os.Getenv("EVENT_GATEWAY")
	c.ClientAddr = addr
	c.User = "arm_event_admin"
	c.Password = "Jv#/TWw3"
	c.Host = "10.4.110.3"
	c.Port = 5432
	c.Database = "arm_event_gateway"
	return nil
}

func (c *config) Addr() string {
	return c.ClientAddr
}

func (c *config) DSN() string {
	return fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable host=%s port=%d", c.User, c.Password, c.Database, c.Host, c.Port)
}

func (c *config) MaxIdleConns() int {
	return 2
}

func (c *config) MaxOpenConns() int {
	return 2
}

func (c *config) ConnMaxIdleTimeSeconds() int {
	return 360
}
