package rest_api_client

import (
	client2 "gitlab.com/danilpan/arm-shared/clients/event-gateway"
	"gitlab.com/danilpan/arm-shared/clients/event-gateway/config"
	"gitlab.com/danilpan/arm-shared/consts"
	deliverypb "gitlab.com/danilpan/arm-shared/proto/delivery"
	pb "gitlab.com/danilpan/arm-shared/proto/delivery"
	utils "gitlab.com/danilpan/arm-shared/utils/delivery"

	"context"
	httpcli "go.unistack.org/micro-client-http/v3"
	jsonpbcodec "go.unistack.org/micro-codec-jsonpb/v3"
	micro_client "go.unistack.org/micro/v3/client"
	"go.unistack.org/micro/v3/codec"
)

type client struct {
	client deliverypb.DeliveryMicroServiceClient
	cfg    *config.ClientConfig
}

func NewEventGatewayClient(cfg *config.ClientConfig) client2.EventGatewayClient {
	return &client{
		cfg: cfg,
	}
}

func (c *client) Init(ctx context.Context) error {
	if c.cfg == nil {
		return consts.ErrNilConfig
	}

	cl := pb.NewDeliveryMicroServiceClient("events-gateway", micro_client.NewClientCallOptions(
		httpcli.NewClient(
			micro_client.ContentType("application/json"),
			micro_client.Codec("text/plain", codec.NewCodec()),
			micro_client.Codec("application/json", jsonpbcodec.NewCodec()),
		),
		micro_client.WithAddress(c.cfg.Addr)))
	c.client = cl

	return nil
}

func (c *client) SendSocketEvent(ctx context.Context, data []byte, identifiers []string, isGlobal bool) (bool, error) {
	req, err := utils.PrepareNotification(data, identifiers, isGlobal)
	if err != nil {
		return false, err
	}

	resp, err := c.client.SendSocketEvent(ctx, req, func(o *micro_client.CallOptions) {
		o.ContentType = "application/json"
	})
	if err != nil {
		return false, err
	}

	if resp != nil {
		return resp.GetStatus(), nil
	}

	return false, nil
}
