package events

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	validSystemEvent = &SystemEvent{
		Action:   ActionReload,
		Endpoint: "some_endpoint",
		Metadata: map[string]interface{}{},
	}
	bts, _ = json.Marshal(validSystemEvent)
)

func TestSystemEvent_ActionRequest(t *testing.T) {
	asserts := assert.New(t)

	validSystemEvent.ActionRequest()

	asserts.Equal(ActionRequest, validSystemEvent.Action)
}

func TestSystemEvent_ActionReload(t *testing.T) {
	asserts := assert.New(t)

	validSystemEvent.ActionReload()

	asserts.Equal(ActionReload, validSystemEvent.Action)
}

func TestSystemEvent_Bytes(t *testing.T) {
	asserts := assert.New(t)
	b, err := validSystemEvent.Bytes()

	asserts.Equal(bts, b)
	asserts.Nil(err)
}
