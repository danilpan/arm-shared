package events

import "encoding/json"

type SystemEvent struct {
	Action   SystemEventAction      `json:"action"`
	Endpoint string                 `json:"endpoint"`
	Metadata map[string]interface{} `json:"metadata"`
}

func (s *SystemEvent) ActionReload() *SystemEvent {
	s.Action = ActionReload
	return s
}

func (s *SystemEvent) ActionRequest() *SystemEvent {
	s.Action = ActionRequest
	return s
}

func (s *SystemEvent) Bytes() ([]byte, error) {
	return json.Marshal(s)
}
