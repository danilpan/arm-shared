package events

type SystemEventAction string

const (
	ActionReload  SystemEventAction = "RELOAD"
	ActionRequest SystemEventAction = "REQUEST"
)
