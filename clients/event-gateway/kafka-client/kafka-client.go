package kafka_client

import (
	"context"
	"encoding/json"
	"fmt"
	client2 "gitlab.com/danilpan/arm-shared/clients/event-gateway"
	"gitlab.com/danilpan/arm-shared/consts"
	"gitlab.com/danilpan/arm-shared/libs/kafka/config"
	"gitlab.com/danilpan/arm-shared/libs/kafka/producer"
	utils "gitlab.com/danilpan/arm-shared/utils/delivery"
)

type client struct {
	pub producer.KafkaPublisher
	cfg *config.ProduceConfig
}

func NewEventGatewayClient(cfg *config.ProduceConfig) client2.EventGatewayClient {
	return &client{
		cfg: cfg,
	}
}

func (c *client) Init(ctx context.Context) error {
	if c.cfg == nil {
		return consts.ErrNilConfig
	}

	pub := producer.NewKafkaPublisher(*c.cfg)

	if err := pub.Init(ctx); err != nil {
		return err
	}

	c.pub = pub

	return nil
}

func (c *client) SendSocketEvent(ctx context.Context, data []byte, identifiers []string, isGlobal bool) (bool, error) {
	req, err := utils.PrepareNotification(data, identifiers, isGlobal)
	if err != nil {
		return false, err
	}

	bts, err := json.Marshal(req)
	if err != nil {
		fmt.Printf("EventGatewayKafkaClient.SendSocketevent. Marshal err. Details: %v", err)
		return false, err
	}

	topic := c.cfg.Topic

	if err = c.pub.Publish(topic, bts); err != nil {
		return false, err
	}

	return true, nil
}
