package event_gateway

import "context"

type EventGatewayClient interface {
	Init(ctx context.Context) error
	SendSocketEvent(ctx context.Context, data []byte, identifiers []string, isGlobal bool) (bool, error)
}
