package database_client

import (
	"context"
	"encoding/json"
	client2 "gitlab.com/danilpan/arm-shared/clients/event-gateway"
	"gitlab.com/danilpan/arm-shared/consts"
	"gitlab.com/danilpan/arm-shared/libs/pg"
	"gitlab.com/danilpan/arm-shared/libs/pg/config"
	utils "gitlab.com/danilpan/arm-shared/utils/delivery"
)

type client struct {
	db  pg.PostgresClient
	cfg *config.DatabaseConfig
}

func NewEventGatewayClient(cfg *config.DatabaseConfig) client2.EventGatewayClient {
	return &client{
		cfg: cfg,
	}
}

func (c *client) Init(ctx context.Context) error {

	if c.cfg == nil {
		return consts.ErrNilConfig
	}

	db := pg.NewPostgresClient(c.cfg)

	if err := db.Connect(ctx); err != nil {
		return err
	}
	c.db = db

	return nil
}

func (c *client) SendSocketEvent(ctx context.Context, data []byte, identifiers []string, isGlobal bool) (bool, error) {
	req, err := utils.PrepareNotification(data, identifiers, isGlobal)
	if err != nil {
		return false, err
	}

	payload, err := json.Marshal(req)

	db := c.db.Database()
	if err != nil {
		return false, err
	}

	_, err = db.ExecContext(ctx, `select pg_notify('notifications','`+string(payload)+"')")
	if err != nil {
		return false, err
	}

	return true, nil
}
