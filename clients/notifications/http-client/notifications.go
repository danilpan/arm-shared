package http_client

import (
	"context"
	"encoding/base64"
	n "gitlab.com/danilpan/arm-shared/clients/notifications"
	config2 "gitlab.com/danilpan/arm-shared/clients/notifications/config"
	pb "gitlab.com/danilpan/arm-shared/proto/notifications"
	httpcli "go.unistack.org/micro-client-http/v3"
	jsonpbcodec "go.unistack.org/micro-codec-jsonpb/v3"
	micro_client "go.unistack.org/micro/v3/client"
	"go.unistack.org/micro/v3/codec"
)

type client struct {
	cfg    *config2.NotificationServiceConfig
	client pb.NotificationsMicroServiceClient
}

func NewEventGatewayClient(cfg *config2.NotificationServiceConfig) n.NotificationsClient {
	return &client{
		cfg: cfg,
	}
}

func (c *client) Init(ctx context.Context) error {

	cl := pb.NewNotificationsMicroServiceClient("notifications", micro_client.NewClientCallOptions(
		httpcli.NewClient(
			micro_client.ContentType("application/json"),
			micro_client.Codec("text/plain", codec.NewCodec()),
			micro_client.Codec("application/json", jsonpbcodec.NewCodec()),
		),
		micro_client.WithAddress(c.cfg.ClientAddr)))
	c.client = cl

	return nil
}

func (c *client) SendNotification(ctx context.Context, message string, options []*pb.SendNotificationRequestOptions) error {

	messageBytes := []byte(message)
	encodedMessage := make([]byte, base64.StdEncoding.EncodedLen(len(messageBytes)))

	base64.StdEncoding.Encode(encodedMessage, messageBytes)

	req := &pb.SendNotificationRequest{
		Options: options,
		Payload: string(encodedMessage),
	}

	_, err := c.client.SendNotification(ctx, req, func(o *micro_client.CallOptions) {
		o.ContentType = "application/json"
	})

	return err
}
