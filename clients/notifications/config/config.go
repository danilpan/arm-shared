package config

type NotificationServiceConfig struct {
	ClientAddr string `json:"client_addr" yaml:"client_addr"`
}

func NewEventGatewayConfig() *NotificationServiceConfig {
	return &NotificationServiceConfig{}
}
