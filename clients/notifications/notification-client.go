package notifications

import (
	"context"
	pb "gitlab.com/danilpan/arm-shared/proto/notifications"
)

type NotificationsClient interface {
	Init(ctx context.Context) error
	SendNotification(ctx context.Context, message string, options []*pb.SendNotificationRequestOptions) error
}
