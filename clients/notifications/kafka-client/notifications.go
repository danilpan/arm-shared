package http_client

import (
	"context"
	"encoding/base64"
	"encoding/json"
	n "gitlab.com/danilpan/arm-shared/clients/notifications"
	"gitlab.com/danilpan/arm-shared/libs/kafka/config"
	"gitlab.com/danilpan/arm-shared/libs/kafka/producer"
	pb "gitlab.com/danilpan/arm-shared/proto/notifications"
)

type client struct {
	cfg *config.ProduceConfig
	pub producer.KafkaPublisher
}

func NewEventGatewayClient(cfg *config.ProduceConfig) n.NotificationsClient {
	return &client{
		cfg: cfg,
		pub: producer.NewKafkaPublisher(*cfg),
	}
}

func (c *client) Init(ctx context.Context) error {

	if err := c.pub.Init(ctx); err != nil {
		return err
	}

	return nil
}

func (c *client) SendNotification(ctx context.Context, message string, options []*pb.SendNotificationRequestOptions) error {
	messageBytes := []byte(message)
	encodedMessage := make([]byte, base64.StdEncoding.EncodedLen(len(messageBytes)))

	base64.StdEncoding.Encode(encodedMessage, messageBytes)

	req := &pb.SendNotificationRequest{
		Options: options,
		Payload: string(encodedMessage),
	}

	bts, err := json.Marshal(req)
	if err != nil {
		return err
	}

	if err = c.pub.Publish(bts); err != nil {
		return err
	}

	return nil
}
