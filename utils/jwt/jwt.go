package jwt

import (
	"github.com/golang-jwt/jwt/v4"
	jwt_models "gitlab.com/danilpan/arm-shared/models/jwt"
)

func ParseJwt(t string) (*jwt.Token, *jwt_models.Claims, error) {
	claims := &jwt_models.Claims{}
	token, _, err := (&jwt.Parser{}).ParseUnverified(t, claims)
	return token, claims, err
}
