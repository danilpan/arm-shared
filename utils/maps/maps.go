package maps

func Keys[K comparable, V any](m map[K]V) []K {
	keys := make([]K, len(m))

	for k, _ := range m {
		keys = append(keys, k)
	}

	return keys
}

// Values - returns slice of values
func Values[K comparable, V any](m map[K]V) []V {
	values := make([]V, len(m))

	for _, v := range m {
		values = append(values, v)
	}

	return values
}

// MappedKeys - returns slice of keys, mapped by specified func
func MappedKeys[K comparable, V any, T any](m map[K]V, mapper func(item K) T) []T {
	keys := make([]T, 0, len(m))

	for k := range m {
		keys = append(keys, mapper(k))
	}

	return keys
}

// MappedValues - returns slice of values, mapped by specified func
func MappedValues[K comparable, V any, T any](m map[K]V, mapper func(item V) T) []T {
	keys := make([]T, 0, len(m))

	for _, v := range m {
		keys = append(keys, mapper(v))
	}

	return keys
}

// Merge - returns new map, which contains unique key-values pairs from specified maps
func Merge[K comparable, V any](maps ...map[K]V) map[K]V {
	result := make(map[K]V)

	for _, m := range maps {
		for k, v := range m {
			result[k] = v
		}
	}

	return result
}

// ForEach casts on each element of map specified func
func ForEach[K comparable, V any](m map[K]V, iteratee func(key K, value V)) {
	for k, v := range m {
		iteratee(k, v)
	}
}

// GetDifference returns map with key-values from mapA, which not in mapB
func GetDifference[K comparable, V any](mapA, mapB map[K]V) map[K]V {
	result := make(map[K]V)

	for k, v := range mapA {
		if _, ok := mapB[k]; !ok {
			result[k] = v
		}
	}
	return result
}
