package enum

type (
	Object struct {
		enums []any
	}
)

func NewObject(enums ...any) *Object {
	return &Object{enums: enums}
}

func (o *Object) Check(value any) bool {
	for _, v := range o.enums {
		if v == value {
			return true
		}
	}
	return false
}
