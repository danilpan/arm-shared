package delivery

import (
	pb "gitlab.com/danilpan/arm-shared/proto/delivery"
	utils "gitlab.com/danilpan/arm-shared/utils/delivery/pb-request"
)

func PrepareNotification(data []byte, identifiers []string, isGlobal bool) (*pb.SendSocketEventRequest, error) {
	dest := utils.SelectorTypeIdentifier()

	if isGlobal {
		dest = utils.SelectorTypeGlobal()
	} else if !isGlobal && len(identifiers) > 1 {
		dest = utils.SelectorTypeList()
	}

	options := utils.NewSendSocketRequestOptions(identifiers, dest)

	return utils.NewSendSocketEventRequest(data, false, options)
}
