package pb_request

import (
	"errors"
	"github.com/stretchr/testify/assert"
	pb "gitlab.com/danilpan/arm-shared/proto/delivery"
	"testing"
)

var (
	data = []string{"role-1", "role-2", "role-3"}

	msg = []byte("something")

	stdOptions = &pb.SendSocketEventRequestOptions{
		Data:         data,
		SelectorType: SelectorTypeList(),
	}

	newSendSocketEventRequestCases = []struct {
		name          string
		msg           []byte
		isSystemEvent bool
		options       *pb.SendSocketEventRequestOptions
		response      *pb.SendSocketEventRequest
		err           error
	}{
		{
			name:          "happy path - notification event",
			msg:           msg,
			isSystemEvent: false,
			options:       stdOptions,
			response: &pb.SendSocketEventRequest{
				Options:   stdOptions,
				Payload:   msg,
				EventType: pb.EventTypes_NOTIFICATION,
			},
			err: nil,
		},
		{
			name:          "happy path - system event",
			msg:           msg,
			isSystemEvent: true,
			options:       stdOptions,
			response: &pb.SendSocketEventRequest{
				Options:   stdOptions,
				Payload:   msg,
				EventType: pb.EventTypes_SYSTEM_EVENT,
			},
			err: nil,
		},
		{
			name:          "err - options are nil",
			msg:           msg,
			isSystemEvent: false,
			options:       nil,
			response:      nil,
			err:           errors.New("options are nil"),
		},
	}
	newSendSocketEventOptionsCases = []struct {
		name         string
		data         []string
		selectorType pb.SelectorType
		response     *pb.SendSocketEventRequestOptions
	}{
		{
			name:         "happy path - data is an array and selector type is list",
			data:         data,
			selectorType: pb.SelectorType_LIST_OF_IDENTIFIERS,
			response: &pb.SendSocketEventRequestOptions{
				Data:         data,
				SelectorType: SelectorTypeList(),
			},
		},
		{
			name:         "happy path - data in nil",
			data:         nil,
			selectorType: pb.SelectorType_LIST_OF_IDENTIFIERS,
			response: &pb.SendSocketEventRequestOptions{
				Data:         nil,
				SelectorType: SelectorTypeList(),
			},
		},
	}
)

func TestNewSendSocketEventRequest(t *testing.T) {
	for _, v := range newSendSocketEventRequestCases {
		t.Run(v.name, func(t *testing.T) {
			resp, err := NewSendSocketEventRequest(v.msg, v.isSystemEvent, v.options)
			assert.Equal(t, v.response, resp)
			if v.err != nil {
				assert.Equal(t, v.err, err)
				assert.Nil(t, resp)
			}
		})
	}
}

func TestNewSendSocketRequestOptions(t *testing.T) {
	for _, v := range newSendSocketEventOptionsCases {
		t.Run(v.name, func(t *testing.T) {
			resp := NewSendSocketRequestOptions(v.data, v.selectorType)
			assert.Equal(t, v.response, resp)
		})
	}
}

func TestSelectorTypeGlobal(t *testing.T) {
	asserts := assert.New(t)
	res := SelectorTypeGlobal()
	asserts.Equal(res, pb.SelectorType_GLOBAL)
}

func TestSelectorTypeIdentifier(t *testing.T) {
	asserts := assert.New(t)
	res := SelectorTypeIdentifier()
	asserts.Equal(res, pb.SelectorType_IDENTIFIER)
}

func TestSelectorTypeList(t *testing.T) {
	asserts := assert.New(t)
	res := SelectorTypeList()
	asserts.Equal(res, pb.SelectorType_LIST_OF_IDENTIFIERS)
}
