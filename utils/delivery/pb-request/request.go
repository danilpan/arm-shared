package pb_request

import (
	"errors"
	pb "gitlab.com/danilpan/arm-shared/proto/delivery"
)

// NewSendSocketEventRequest for new socket response creation.
// Data is a message which you want to send
// isSystemEvent should be false for Systems's internal events. For notification use true
// Options are required field which contains information about respondents
func NewSendSocketEventRequest(data []byte, isSystemEvent bool, options *pb.SendSocketEventRequestOptions) (*pb.SendSocketEventRequest, error) {
	if options == nil {
		return nil, errors.New("options are nil")
	}
	eventType := pb.EventTypes_NOTIFICATION

	if isSystemEvent {
		eventType = pb.EventTypes_SYSTEM_EVENT
	}
	r := &pb.SendSocketEventRequest{
		EventType: eventType,
		Payload:   data,
		Options:   options,
	}
	return r, nil
}

// NewSendSocketRequestOptions is a wrapper for SendSocketRequestOptions creation.
// You can use it to avoid pb imports
func NewSendSocketRequestOptions(selectors []string, t pb.SelectorType) *pb.SendSocketEventRequestOptions {
	o := &pb.SendSocketEventRequestOptions{
		SelectorType: t,
		Data:         selectors,
	}
	return o
}

// SelectorTypeGlobal is a func which using to avoid pb imports
func SelectorTypeGlobal() pb.SelectorType {
	return pb.SelectorType_GLOBAL
}

// SelectorTypeIdentifier is a func which using to avoid pb imports
func SelectorTypeIdentifier() pb.SelectorType {
	return pb.SelectorType_IDENTIFIER
}

// SelectorTypeList is a func which using to avoid pb imports
func SelectorTypeList() pb.SelectorType {
	return pb.SelectorType_LIST_OF_IDENTIFIERS
}
