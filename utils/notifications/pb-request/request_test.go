package pb_request

import (
	"errors"
	"github.com/stretchr/testify/assert"
	pb "gitlab.com/danilpan/arm-shared/proto/notifications"
	"testing"
)

var (
	data = []string{"role-1", "role-2", "role-3"}

	msg = "something"

	system = "some-system"

	selectors = []pb.SelectorType{pb.SelectorType_ROLES_LIST, pb.SelectorType_BRANCHES_LIST}

	stdOptions = []*pb.SendNotificationRequestOptions{
		{
			Data:     data,
			Selector: pb.SelectorType_ROLES_LIST,
		},
	}

	newSendSocketEventRequestCases = []struct {
		name     string
		msg      string
		system   string
		options  []*pb.SendNotificationRequestOptions
		response *pb.SendNotificationRequest
		err      error
	}{
		{
			name:    "happy path - notification event",
			msg:     msg,
			system:  system,
			options: stdOptions,
			response: &pb.SendNotificationRequest{
				Options: stdOptions,
				Payload: msg,
				System:  system,
			},
			err: nil,
		},
		{
			name:    "happy path - system event",
			msg:     msg,
			system:  system,
			options: stdOptions,
			response: &pb.SendNotificationRequest{
				Options: stdOptions,
				Payload: msg,
				System:  system,
			},
			err: nil,
		},
		{
			name:     "err - options are nil",
			msg:      msg,
			system:   system,
			options:  nil,
			response: nil,
			err:      errors.New("options are empty"),
		},
		{
			name:     "err - options are empty",
			msg:      msg,
			system:   system,
			options:  make([]*pb.SendNotificationRequestOptions, 0),
			response: nil,
			err:      errors.New("options are empty"),
		},
		{
			name:     "err - system is empty",
			msg:      msg,
			system:   "",
			options:  make([]*pb.SendNotificationRequestOptions, 10),
			response: nil,
			err:      errors.New("system is empty"),
		},
	}
	newSendSocketEventOptionsCases = []struct {
		name     string
		data     []string
		selector pb.SelectorType
		response *pb.SendNotificationRequestOptions
	}{
		{
			name:     "happy path - data is an array and selector type is list",
			data:     data,
			selector: pb.SelectorType_ROLES_LIST,
			response: &pb.SendNotificationRequestOptions{
				Data:     data,
				Selector: pb.SelectorType_ROLES_LIST,
			},
		},
		{
			name:     "happy path - data in nil",
			data:     nil,
			selector: pb.SelectorType_ROLES_LIST,
			response: &pb.SendNotificationRequestOptions{
				Data:     nil,
				Selector: pb.SelectorType_ROLES_LIST,
			},
		},
	}
)

func TestNewSendSocketEventRequest(t *testing.T) {
	for _, v := range newSendSocketEventRequestCases {
		t.Run(v.name, func(t *testing.T) {
			resp, err := NewSendNotificationRequest(v.msg, v.system, v.options...)
			assert.Equal(t, v.response, resp)
			if v.err != nil {
				assert.Equal(t, v.err, err)
				assert.Nil(t, resp)
			}
		})
	}
}

func TestNewSendSocketRequestOptions(t *testing.T) {
	for _, v := range newSendSocketEventOptionsCases {
		t.Run(v.name, func(t *testing.T) {
			resp := NewNotificationRequestOptions(v.data, v.selector)
			assert.Equal(t, v.response, resp)
		})
	}
}

func TestSelectorTypeGlobal(t *testing.T) {
	asserts := assert.New(t)
	res := SelectorTypeGlobal()
	asserts.Equal(res, pb.SelectorType_GLOBAL)
}

func TestSelectorTypeIdentifier(t *testing.T) {
	asserts := assert.New(t)
	res := SelectorTypeIdentifier()
	asserts.Equal(res, pb.SelectorType_IDENTIFIER)
}

func TestSelectorTypeRolesList(t *testing.T) {
	asserts := assert.New(t)
	res := SelectorTypeRolesList()
	asserts.Equal(res, pb.SelectorType_ROLES_LIST)
}

func TestSelectorTypeBranchesList(t *testing.T) {
	asserts := assert.New(t)
	res := SelectorTypeBranchesList()
	asserts.Equal(res, pb.SelectorType_BRANCHES_LIST)
}

func TestSelectorTypeDepartmentsList(t *testing.T) {
	asserts := assert.New(t)
	res := SelectorTypeDepartmentsList()
	asserts.Equal(res, pb.SelectorType_DEPARTMENTS_LIST)
}
