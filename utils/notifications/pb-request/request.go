package pb_request

import (
	"errors"
	pb "gitlab.com/danilpan/arm-shared/proto/notifications"
)

// NewSendNotificationRequest for new socket response creation.
// Data is a message which you want to send
// Options are required field which contains information about users search params
func NewSendNotificationRequest(data string, system string, options ...*pb.SendNotificationRequestOptions) (*pb.SendNotificationRequest, error) {
	if options == nil || len(options) == 0 {
		return nil, errors.New("options are empty")
	}

	if system == "" {
		return nil, errors.New("system is empty")
	}

	r := &pb.SendNotificationRequest{
		Payload: data,
		Options: options,
		System:  system,
	}
	return r, nil
}

// NewNotificationRequestOptions is a wrapper for SendSocketRequestOptions creation.
// You can use it to avoid pb imports
func NewNotificationRequestOptions(selectors []string, t pb.SelectorType) *pb.SendNotificationRequestOptions {
	o := &pb.SendNotificationRequestOptions{
		Selector: t,
		Data:     selectors,
	}
	return o
}

// SelectorTypeGlobal is a func which using to avoid pb imports
func SelectorTypeGlobal() pb.SelectorType {
	return pb.SelectorType_GLOBAL
}

// SelectorTypeIdentifier is a func which using to avoid pb imports
func SelectorTypeIdentifier() pb.SelectorType {
	return pb.SelectorType_IDENTIFIER
}

// SelectorTypeRolesList is a func which using to avoid pb imports
func SelectorTypeRolesList() pb.SelectorType {
	return pb.SelectorType_ROLES_LIST
}

// SelectorTypeBranchesList is a func which using to avoid pb imports
func SelectorTypeBranchesList() pb.SelectorType {
	return pb.SelectorType_BRANCHES_LIST
}

// SelectorTypeDepartmentsList is a func which using to avoid pb imports
func SelectorTypeDepartmentsList() pb.SelectorType {
	return pb.SelectorType_DEPARTMENTS_LIST
}
