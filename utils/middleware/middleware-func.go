package middleware

import (
	"context"
	"net/http"
)

type hldr struct {
	fn http.HandlerFunc
}

func (h *hldr) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.fn(w, r)
}

type namedRouteKey struct{}

type HTTPMiddlewareFunc func(handler http.Handler) http.Handler

func SetRouteName(name string, mws []HTTPMiddlewareFunc, fn http.HandlerFunc) http.HandlerFunc {
	var h http.Handler
	h = &hldr{fn}

	for i := len(mws) - 1; i >= 0; i-- {
		h = mws[i](h)
	}
	/*
		for i := len(mws); i > 0; i-- {
			h = mws[i-1](h)
		}
	*/

	nfn := func(w http.ResponseWriter, r *http.Request) {
		r = r.WithContext(context.WithValue(r.Context(), namedRouteKey{}, name))
		h.ServeHTTP(w, r)
	}

	return nfn
}

func GetRouteName(ctx context.Context) string {
	v, ok := ctx.Value(namedRouteKey{}).(string)
	if !ok {
		v = "unknown"
	}
	return v
}
