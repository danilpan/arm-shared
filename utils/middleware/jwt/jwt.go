package jwt

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/danilpan/arm-logger/types/field"
	context2 "gitlab.com/danilpan/arm-shared/models/context"
	"gitlab.com/danilpan/arm-shared/models/response"
	"gitlab.com/danilpan/arm-shared/utils/jwt"
	"net/http"
	"strings"
)

const (
	AUTH_HEADER                  = "Authorization"
	CONTENT_TYPE_APPICATION_JSON = "application/json"
)

type JwtMiddlewareAgent struct {
	lg *logger.Logger
}

func NewJWTMiddlewareAgent(lg *logger.Logger) *JwtMiddlewareAgent {
	return &JwtMiddlewareAgent{
		lg: lg,
	}
}

func (a *JwtMiddlewareAgent) CheckJwtHttpMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		lg := a.lg.WithFields(field.StringField("Middleware", "Jwt"))

		ctx := r.Context()
		resp := &response.ServiceResponse{
			Method:   r.Method,
			Endpoint: r.URL.String(),
		}

		token := r.Header.Get(AUTH_HEADER)

		if token == "" {
			lg.Error().Print("auth token is not presented")
			w.Header().Set("Content-Type", CONTENT_TYPE_APPICATION_JSON)

			resp.SetError(errors.New("auth token is not presented"))
			resp.Status = "Unauthorized"
			bts, _ := json.Marshal(resp)

			w.WriteHeader(http.StatusUnauthorized)
			w.Write(bts)
			return
		}

		splitted := strings.Split(token, " ")

		if len(splitted) < 2 {
			err := errors.New("header consists of wrong number of segments")

			lg.WithError(err).Print("bad header")
			w.Header().Set("Content-Type", CONTENT_TYPE_APPICATION_JSON)
			resp.SetError(err)
			resp.Status = "Unauthorized"
			bts, _ := json.Marshal(resp)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write(bts)
			return
		}

		token = splitted[1]

		_, claims, err := jwt.ParseJwt(token)
		if err != nil {
			lg.WithError(err).Print("can not parse jwt token")

			w.Header().Set("Content-Type", CONTENT_TYPE_APPICATION_JSON)
			resp.SetError(fmt.Errorf("token parse failed. Details: %s", err.Error()))
			resp.Status = "Unauthorized"
			bts, _ := json.Marshal(resp)

			w.WriteHeader(http.StatusUnauthorized)
			w.Write(bts)
			return
		}

		lg.Info().Print("jwt token is correct")
		ctx = context.WithValue(ctx, context2.ClaimsKey{}, claims)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (a *JwtMiddlewareAgent) CheckJwtEchoMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		lg := a.lg.WithFields(field.StringField("Middleware", "Jwt"))
		ctx := c.Request().Context()
		resp := &response.ServiceResponse{
			Method:   c.Request().Method,
			Endpoint: c.Request().URL.String(),
		}

		token := c.Request().Header.Get(AUTH_HEADER)

		if token == "" {
			lg.Error().Print("auth token is not presented")
			resp.SetError(errors.New("auth token is not presented"))
			resp.Status = "Unauthorized"
			return c.JSON(http.StatusUnauthorized, resp)
		}

		splitted := strings.Split(token, " ")

		if len(splitted) < 2 {
			err := errors.New("header consists of wrong number of segments")
			lg.WithError(err).Print("bad header")

			resp.Status = "BadRequest"
			return c.JSON(http.StatusBadRequest, resp)
		}

		token = splitted[1]

		_, claims, err := jwt.ParseJwt(token)
		if err != nil {
			lg.WithError(err).Print("can not parse jwt token")

			resp.Status = "Unauthorized"
			return c.JSON(http.StatusUnauthorized, resp)
		}

		lg.Info().Print("jwt token is correct")
		ctx = context.WithValue(ctx, context2.ClaimsKey{}, claims)
		c.SetRequest(c.Request().WithContext(ctx))
		return next(c)
	}
}

func (a *JwtMiddlewareAgent) CheckJwtGinMiddleware(c *gin.Context) {
	lg := a.lg.WithFields(field.StringField("Middleware", "Jwt"))

	ctx := c.Request.Context()
	resp := &response.ServiceResponse{
		Method:   c.Request.Method,
		Endpoint: c.Request.URL.String(),
	}

	token := c.Request.Header.Get(AUTH_HEADER)

	if token == "" {
		lg.Error().Print("auth token is not presented")
		resp.SetError(errors.New("auth token is not presented"))
		resp.Status = "Unauthorized"
		c.JSON(http.StatusUnauthorized, resp)
	}

	splitted := strings.Split(token, " ")

	if len(splitted) < 2 {
		err := errors.New("header consists of wrong number of segments")
		lg.WithError(err).Print("bad header")

		resp.Status = "Unauthorized"
		c.JSON(http.StatusUnauthorized, resp)
	}

	token = splitted[1]

	_, claims, err := jwt.ParseJwt(token)
	if err != nil {
		lg.WithError(err).Print("can not parse jwt token")
		resp.Status = "Unauthorized"
		c.JSON(http.StatusUnauthorized, resp)
	}

	lg.Info().Print("jwt token is correct")
	ctx = context.WithValue(ctx, context2.ClaimsKey{}, claims)
	c.Request = c.Request.WithContext(ctx)
	c.Next()
}
