package consts

import "errors"

var (
	ErrNilConfig = errors.New("config is nil")
)
