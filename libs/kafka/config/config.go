package config

type ProduceConfig struct {
	Addrs         []string `json:"addrs" yaml:"addrs"`
	User          string   `json:"user" yaml:"user"`
	Password      string   `json:"password" yaml:"password"`
	ReturnSuccess bool     `json:"return_success" yaml:"return_success" default:"true"`
	Enable        bool     `json:"enable" yaml:"enable" default:"true"`
	Topic         string   `json:"topic" yaml:"topic"`
}

type ConsumerConfig struct {
	Addrs         []string `json:"addrs" yaml:"addrs"`
	User          string   `json:"user" yaml:"user"`
	Password      string   `json:"password" yaml:"password"`
	InitialOffset int64    `json:"initial_offset" yaml:"initial_offset"`
	Enable        bool     `json:"enable" default:"true" yaml:"enable"`
	Topic         string   `json:"topic" yaml:"topic"`
}
