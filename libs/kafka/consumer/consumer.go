package consumer

import (
	"context"
	"github.com/ThreeDotsLabs/watermill"
	kf "github.com/ThreeDotsLabs/watermill-kafka/v2/pkg/kafka"
	"github.com/ThreeDotsLabs/watermill/message"
	"gitlab.com/danilpan/arm-shared/libs/kafka/config"
)

type SubscribeHandler func(m *message.Message) error

type KafkaSubscriber interface {
	Init(ctx context.Context) error
	Listen(ctx context.Context) error
	Close(ctx context.Context) error
	WithOverridingConfig(cfg kf.SubscriberConfig) KafkaSubscriber
}

type sub struct {
	topic      string
	cfg        kf.SubscriberConfig
	subscriber message.Subscriber
	handler    SubscribeHandler
}

func NewKafkaSubscriber(cfg config.ConsumerConfig, handler SubscribeHandler) KafkaSubscriber {
	defaultConfig := kf.DefaultSaramaSubscriberConfig()
	defaultConfig.Consumer.Offsets.Initial = cfg.InitialOffset
	defaultConfig.Net.SASL.User = cfg.User
	defaultConfig.Net.SASL.Password = cfg.Password
	defaultConfig.Net.SASL.Enable = cfg.Enable

	return &sub{
		cfg: kf.SubscriberConfig{
			Brokers:               cfg.Addrs,
			Unmarshaler:           kf.DefaultMarshaler{},
			OverwriteSaramaConfig: defaultConfig,
		},
		handler: handler,
		topic:   cfg.Topic,
	}
}

func (s *sub) WithOverridingConfig(cfg kf.SubscriberConfig) KafkaSubscriber {
	s.cfg = cfg
	return s
}

func (s *sub) Init(ctx context.Context) error {
	sb, err := kf.NewSubscriber(
		s.cfg,
		watermill.NewStdLogger(false, false),
	)
	if err != nil {
		return err
	}

	s.subscriber = sb

	return nil
}

func (s *sub) Listen(ctx context.Context) error {
	ch, err := s.subscriber.Subscribe(ctx, s.topic)
	if err != nil {
		return err
	}

loop:
	for {
		select {
		case vl := <-ctx.Done():
			_ = vl
			break loop

		case msg, ok := <-ch:
			if !ok {
				break loop
			}

			go s.handler(msg)
		}
	}
	return s.subscriber.Close()
}

func (s *sub) Close(ctx context.Context) error {
	return s.subscriber.Close()
}
