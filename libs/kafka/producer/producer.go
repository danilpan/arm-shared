package producer

import (
	"context"
	"github.com/ThreeDotsLabs/watermill"
	kf "github.com/ThreeDotsLabs/watermill-kafka/v2/pkg/kafka"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/google/uuid"
	"gitlab.com/danilpan/arm-shared/libs/kafka/config"
)

type KafkaPublisher interface {
	Init(ctx context.Context) error
	Publish(payload []byte) error
	WithOverridingConfig(cfg kf.PublisherConfig) KafkaPublisher
}

type pub struct {
	cfg       kf.PublisherConfig
	publisher message.Publisher
	topic     string
}

func NewKafkaPublisher(cfg config.ProduceConfig) KafkaPublisher {
	saramaSubscriberConfig := kf.DefaultSaramaSyncPublisherConfig()

	saramaSubscriberConfig.Producer.Return.Successes = cfg.ReturnSuccess
	saramaSubscriberConfig.Net.SASL.User = cfg.User
	saramaSubscriberConfig.Net.SASL.Password = cfg.Password
	saramaSubscriberConfig.Net.SASL.Enable = cfg.Enable

	return &pub{
		cfg: kf.PublisherConfig{
			Brokers:               cfg.Addrs,
			Marshaler:             kf.DefaultMarshaler{},
			OverwriteSaramaConfig: saramaSubscriberConfig,
		},
		topic: cfg.Topic,
	}
}

func (p *pub) WithOverridingConfig(cfg kf.PublisherConfig) KafkaPublisher {
	p.cfg = cfg
	return p
}

func (p *pub) Init(ctx context.Context) error {
	pb, err := kf.NewPublisher(
		p.cfg,
		watermill.NewStdLogger(false, false),
	)
	if err != nil {
		return err
	}

	p.publisher = pb

	return nil
}

func (p *pub) Publish(payload []byte) error {
	uid := uuid.New()
	m := message.NewMessage(uid.String(), payload)

	return p.publisher.Publish(p.topic, m)
}
