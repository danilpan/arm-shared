package redis

import (
	"context"
	"errors"
	"gitlab.com/danilpan/arm-shared/libs/redis/config"
	"time"
)

var (
	defaultExpiration = time.Minute * 5
)

type RedisClient interface {
	Connect(ctx context.Context) error
	Get(ctx context.Context, key string, dest interface{}) error
	MGet(ctx context.Context, keys []string, dest interface{}) error
	Set(ctx context.Context, key string, val interface{}, expiration *time.Duration) error
	MSet(ctx context.Context, vals map[string]interface{}) error
	Delete(ctx context.Context, keys ...string) error
	Exists(ctx context.Context, key string) error
	Close() error
}

type client struct {
	cfg *config.RedisConfig
	r   *redis.Client
}

func NewRedisClient(cfg *config.RedisConfig) RedisClient {
	return &client{
		cfg: cfg,
	}
}

func (c *client) Connect(ctx context.Context) error {
	rdb := redis.NewClient(&redis.Options{
		Addr:     c.cfg.Addr,
		Password: c.cfg.Password, // no password set
		Username: c.cfg.Username,
		DB:       c.cfg.Database, // use default DB
	})

	if c.cfg.Expiration != 0 {
		defaultExpiration = time.Duration(c.cfg.Expiration) * time.Second
	}

	if _, err := rdb.Ping(ctx).Result(); err != nil {
		return err
	}

	c.r = rdb

	return nil
}

func (c *client) Get(ctx context.Context, key string, dest interface{}) error {
	return c.r.Get(ctx, key).Scan(dest)
}

func (c *client) MGet(ctx context.Context, keys []string, dest interface{}) error {
	return c.r.MGet(ctx, keys...).Scan(dest)
}

func (c *client) Set(ctx context.Context, key string, val interface{}, expiration *time.Duration) error {

	var exp time.Duration

	if expiration != nil {
		exp = *expiration
	} else {
		exp = defaultExpiration
	}

	r := c.r.Set(ctx, key, val, exp)
	return r.Err()
}

func (c *client) MSet(ctx context.Context, vals map[string]interface{}) error {
	r := c.r.MSet(ctx, vals, defaultExpiration)
	return r.Err()
}

func (c *client) Delete(ctx context.Context, keys ...string) error {
	return c.r.Del(ctx, keys...).Err()
}

func (c *client) Close() error {
	return c.r.Close()
}

func (c *client) Exists(ctx context.Context, key string) error {
	exists, err := c.r.Exists(ctx, key).Result()
	if err != nil {
		return err
	}

	if exists == 0 {
		return errors.New("not found")
	}

	return nil
}
