package config

type RedisConfig struct {
	Username   string `json:"user" yaml:"user"`
	Password   string `json:"password" yaml:"password"`
	Addr       string `json:"addr" yaml:"addr"`
	Database   int    `json:"database" yaml:"database"`
	Expiration int64  `json:"expiration" yaml:"expiration"` // In Seconds
}
