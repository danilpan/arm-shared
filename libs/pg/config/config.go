package config

import "fmt"

var (
	connectionStringLayout = "user=%s password=%s dbname=%s sslmode=disable host=%s port=%s"
)

type ConnectionOptions struct {
	MaxIdleConnectionCount int `json:"max_idle_conns" yaml:"max_idle_conns" default:"1"`
	MaxOpenConnectionCount int `json:"max_open_conns" yaml:"max_open_conns" default:"1"`
	ConnectionMaxIdleTime  int `json:"conn_max_idle_time" yaml:"conn_max_idle_time" default:"1"`
}

type DatabaseConfig struct {
	User              string            `json:"user" yaml:"user"`
	Password          string            `json:"password" yaml:"password"`
	Host              string            `json:"host" yaml:"host"`
	Port              string            `json:"port" yaml:"port"`
	Database          string            `json:"database" yaml:"database"`
	ConnectionOptions ConnectionOptions `json:"connection_options" yaml:"connection_options"`
}

func (c *DatabaseConfig) String() string {
	return fmt.Sprintf(connectionStringLayout, c.User, c.Password, c.Database, c.Host, c.Port)
}
