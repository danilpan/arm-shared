package pg

import (
	"context"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/danilpan/arm-shared/consts"
	"gitlab.com/danilpan/arm-shared/libs/pg/config"
	"time"
)

const driver = "postgres"

var (
	DefaultClient PostgresClient
)

type PostgresClient interface {
	Connect(ctx context.Context) error
	Database() *sqlx.DB
	IsConnected() bool
	Close() error
}

type client struct {
	cfg       *config.DatabaseConfig
	db        *sqlx.DB
	connected bool
}

func NewPostgresClient(cfg *config.DatabaseConfig) PostgresClient {
	c := &client{
		cfg: cfg,
	}
	return c

}

func (c *client) Connect(ctx context.Context) error {
	if c.cfg == nil {
		return consts.ErrNilConfig
	}

	db, errConnect := sqlx.ConnectContext(ctx, driver, c.cfg.String())
	if errConnect != nil {
		return errConnect
	}

	opts := c.cfg.ConnectionOptions

	db.SetMaxIdleConns(opts.MaxIdleConnectionCount)
	db.SetMaxOpenConns(opts.MaxOpenConnectionCount)
	db.SetConnMaxIdleTime(time.Duration(opts.ConnectionMaxIdleTime) * time.Second)
	c.db = db
	c.connected = true

	return nil
}

func (c *client) IsConnected() bool {
	return c.connected
}

func (c *client) Database() *sqlx.DB {
	return c.db
}

func (c *client) Close() error {
	return c.db.Close()
}
