package jwt

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	NameAttr = CustomEntryAttr{
		Name:   "cn",
		Values: []string{"Руслан Галиев"},
	}
	EmployeeId = CustomEntryAttr{
		Name:   "employeeID",
		Values: []string{"017817"},
	}
	sAMAccountName = CustomEntryAttr{
		Name:   "sAMAccountName",
		Values: []string{"u16310"},
	}
	mail = CustomEntryAttr{
		Name:   "mail",
		Values: []string{"Ruslan.Galiev@berekebank.kz"},
	}
	customEntry = CustomEntry{
		DN: "CN=Руслан Галиев,OU=000-114-ДРТ Департамент развития технологий,OU=ДБ AO Cбepбaнк,OU=SB Sberbank JSC,DC=sberbank,DC=kz",
		Attrs: []CustomEntryAttr{
			NameAttr, EmployeeId, sAMAccountName, mail,
		},
	}
	custom = &Custom{
		Username: "u16310",
		Entry:    customEntry,
	}

	token = "{\"username\":\"u16310\",\"entry\":{\"DN\":\"CN=Руслан Галиев,OU=000-114-ДРТ Департамент развития технологий,OU=ДБ AO Cбepбaнк,OU=SB Sberbank JSC,DC=sberbank,DC=kz\",\"Attributes\":[{\"Name\":\"cn\",\"Values\":[\"Руслан Галиев\"],\"ByteValues\":[\"0KDRg9GB0LvQsNC9INCT0LDQu9C40LXQsg==\"]},{\"Name\":\"employeeID\",\"Values\":[\"017817\"],\"ByteValues\":[\"MDE3ODE3\"]},{\"Name\":\"sAMAccountName\",\"Values\":[\"u16310\"],\"ByteValues\":[\"dTE2MzEw\"]},{\"Name\":\"mail\",\"Values\":[\"Ruslan.Galiev@berekebank.kz\"],\"ByteValues\":[\"UnVzbGFuLkdhbGlldkBiZXJla2ViYW5rLmt6\"]}]}}"
)

func TestParseCustomClaims(t *testing.T) {
	asserts := assert.New(t)

	c, err := ParseCustomClaims(token)

	asserts.Nil(err)
	asserts.Equal(custom, c)
}
