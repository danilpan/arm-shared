package jwt

import (
	"crypto/subtle"
	"encoding/json"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

type Claims struct {
	// the `iss` (Issuer) claim. See https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.1
	Issuer string `json:"iss,omitempty"`

	// the `sub` (Subject) claim. See https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.2
	Subject string `json:"sub,omitempty"`

	// the `aud` (Audience) claim. See https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.3
	Audience jwt.ClaimStrings `json:"aud,omitempty"`

	// the `exp` (Expiration Time) claim. See https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.4
	ExpiresAt *jwt.NumericDate `json:"exp,omitempty"`

	// the `nbf` (Not Before) claim. See https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.5
	// NotBefore *jwt.NumericDate `json:"nbf,omitempty"`

	// the `iat` (Issued At) claim. See https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.6
	// IssuedAt *jwt.NumericDate `json:"iat,omitempty"`

	// the `jti` (JWT ID) claim. See https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.7
	ID string `json:"jti,omitempty"`

	// the `kid` (Key ID) claim
	// KID string `json:"kid,omitempty"`

	RealmAccess *Realm `json:"realm_access,omitempty"`

	Roles []string `json:"roles,omitempty"`

	Custom interface{} `json:"custom,omitempty"`
}

type Realm struct {
	Roles []string `json:"roles,omitempty"`
}

type Custom struct {
	Username string      `json:"username"`
	Entry    CustomEntry `json:"entry"`
}

type CustomEntry struct {
	DN    string            `json:"DN"`
	Attrs []CustomEntryAttr `json:"Attributes"`
}

type CustomEntryAttr struct {
	Name   string   `json:"Name"`
	Values []string `json:"Values"`
}

func (c *Custom) Valid() error {
	return nil
}

func ParseCustomClaims(c string) (*Custom, error) {
	custom := &Custom{}

	err := json.Unmarshal([]byte(c), &custom)

	return custom, err
}

func (c *Claims) Valid() error {
	vErr := new(jwt.ValidationError)
	now := jwt.TimeFunc().Unix()

	if !c.VerifyExpiresAt(now, false) {
		vErr.Inner = jwt.ErrTokenExpired
		vErr.Errors |= jwt.ValidationErrorExpired
	}

	if vErr.Errors == 0 {
		return nil
	}

	return vErr
}

// VerifyExpiresAt compares the exp claim against cmp (cmp <= exp).
// If req is false, it will return true, if exp is unset.
func (c *Claims) VerifyExpiresAt(cmp int64, req bool) bool {
	cmpTime := time.Unix(cmp, 0)
	return verifyExp(c.ExpiresAt, cmpTime, req)
}

// VerifyIssuer compares the iss claim against cmp.
// If required is false, this method will return true if the value matches or is unset
func (c *Claims) VerifyIssuer(cmp string, req bool) bool {
	return verifyIss(c.Issuer, cmp, req)
}

func (c *Claims) RoleValues() []string {
	if len(c.Roles) == 0 {
		return []string{}
	}

	roles := make([]string, len(c.Roles))
	for k, v := range c.Roles {
		roles[k] = extractRole(v)
	}

	return roles
}

func verifyExp(exp *jwt.NumericDate, now time.Time, req bool) bool {
	if exp == nil {
		return !req
	}

	return now.Before(exp.Time)
}

func verifyIss(iss string, cmp string, required bool) bool {
	if iss == "" {
		return !required
	}
	return subtle.ConstantTimeCompare([]byte(iss), []byte(cmp)) != 0
}

func extractRole(str string) string {
	parts := strings.Split(str, ",")

	for _, v := range parts {
		if strings.HasPrefix(v, "CN=") {
			//result := r.FindAllString(string(v[3:]), -1)
			//if len(result) == 0 {
			//	return ""
			//}
			return string(v[3:])
		}
	}

	return ""
}
