package context

type (
	//ClaimsKey is a key for Jwt's claims
	ClaimsKey struct{}

	//UserClaims is a key for claims casted to models/jwt/Custom type
	UserClaims struct{}

	//TokenKey is a key for Jwt token
	TokenKey struct{}

	//UserIDKey is a key for user's ID
	UserIDKey struct{}

	//RequestId is a key for request id storing
	RequestId struct{}
)
