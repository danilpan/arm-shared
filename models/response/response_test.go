package response

import (
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	requestId     = "123"
	method        = "GET"
	endpoint      = "/some_endpoint"
	err           = errors.New("some error")
	errorBytes, _ = json.Marshal(&serviceError{
		Error: err.Error(),
	})
	serviceResponseWithError = &ServiceResponse{
		Endpoint:  endpoint,
		Method:    method,
		Status:    STATUS_FAILED,
		RequestID: requestId,
		Payload:   errorBytes,
	}

	serviceResponseWithoutError = &ServiceResponse{
		Endpoint:  endpoint,
		Method:    method,
		Status:    STATUS_OK,
		RequestID: requestId,
		Payload:   nil,
	}

	testCases = []struct {
		name     string
		expected *ServiceResponse
		err      error
	}{
		{
			name:     "happy path",
			expected: serviceResponseWithoutError,
			err:      nil,
		},
		{
			name:     "error expected",
			expected: serviceResponseWithError,
			err:      err,
		},
	}
)

func TestServiceResponse_SetError(t *testing.T) {

	for _, v := range testCases {
		t.Run(v.name, func(t *testing.T) {
			asserts := assert.New(t)

			rs := &ServiceResponse{
				Endpoint:  endpoint,
				Method:    method,
				Status:    STATUS_OK,
				RequestID: requestId,
			}

			rs.SetError(v.err)

			asserts.Equal(v.expected, rs)

		})
	}
}
