package response

import (
	"encoding/json"
	"gitlab.com/danilpan/arm-shared/models/context"
	"net/http"
)

const (
	STATUS_OK     = "ok"
	STATUS_FAILED = "failed"
)

type ServiceResponse struct {
	Endpoint  string            `json:"endpoint"`
	Method    string            `json:"httpMethod"`
	Metadata  map[string]string `json:"metadata,omitempty"`
	Status    string            `json:"status,omitempty"`
	RequestID string            `json:"requestId,omitempty"`
	Payload   json.RawMessage   `json:"payload,omitempty"`
}

type serviceError struct {
	Error string `json:"err"`
}

func NewServiceResponse(endpoint string, method string, requestId string, body interface{}) (*ServiceResponse, error) {
	rs := &ServiceResponse{
		Endpoint:  endpoint,
		Method:    method,
		RequestID: requestId,
	}

	if body != nil {
		bts, err := json.Marshal(body)
		if err != nil {
			return rs, err
		}
		rs.Payload = bts
	}

	return rs, nil
}

func (s *ServiceResponse) FromRequest(r *http.Request) {
	s.Endpoint = r.URL.String()
	s.Method = r.Method

	if reqId := r.Context().Value(context.RequestId{}); reqId != nil {
		if reqIdStr, ok := reqId.(string); ok {
			s.RequestID = reqIdStr
		}
	}
}

func (s *ServiceResponse) SetError(e error) {
	if e == nil {
		return
	}
	err := serviceError{
		Error: e.Error(),
	}
	bts, _ := json.Marshal(&err)
	s.Payload = bts
	s.Status = STATUS_FAILED
}

func (s *ServiceResponse) SendResponse(w http.ResponseWriter, code int) {
	data, err := json.Marshal(s)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(code)
	w.Write(data)
}
