#!/bin/sh -ex

INC=" -I$(go list -f '{{ .Dir }}' -m go.unistack.org/micro/v3) -I$(go list -f '{{ .Dir }}' -m go.unistack.org/micro-proto/v3)"

for dir in proto/delivery proto/notifications; do
  #protoc -I./${dir} $INC --openapiv2_out=disable_default_errors=true,allow_merge=true:./${dir} --go_out=paths=source_relative:./${dir} ${dir}/*.proto
  protoc -I./${dir} $INC --go_out=paths=source_relative:./${dir} ${dir}/*.proto
  protoc -I./${dir} $INC --go-micro_out=components="micro|http",debug=true,tag_path=./${dir},paths=source_relative:./${dir} ${dir}/*.proto
  protoc -I./${dir} $INC --go-micro_out=components="openapiv3",openapi_file=./apidocs.swagger.yaml,debug=true,paths=source_relative:./${dir} ${dir}/*.proto
done